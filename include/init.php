<?php
	ini_set('display_errors', true);
	
	error_reporting(E_ALL ^ E_NOTICE ^ E_USER_NOTICE ^ E_DEPRECATED ^ E_STRICT);

	define('WEBSITE_ENCODING', 'UTF-8');
	
	require_once 'include/functions.php';

	date_default_timezone_set('Europe/Amsterdam');

	/* Import composer packages */
	require_once dirname(__FILE__) . '/../vendor/autoload.php';

	/* Initialize session */
	session_start();
