<?php
$countdown = get_config_value('countdown');

if (empty($countdown)) {
    http_response_code( 204 ); // no content
    die();
}

$now = new DateTime();
$deadline = new DateTime($countdown['timestamp']);
$timeToGo = $now->diff($deadline);

if ($now > $deadline) {
    http_response_code( 204 ); // no content
    die();
}

?>

<style>
    #countdownslide.container { 
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #countdownslide .center {
        font-size: 6vw;
        line-height: 1em;
        text-align: center;
    }

    #countdownslide .title {
        margin-bottom: .5em;
    }
</style>

<div id="countdownslide" class="container">
  <div class="center">
        <div class="title"><?= $countdown['text'] ?></div>
        <div id="countdown">
            <?= $now > $deadline ? 'EXPIRED' : $timeToGo->format('%d days, %h:%i:%s') ?>
        </div>
    </div>
</div>

<!-- Modified from https://www.w3schools.com/howto/howto_js_countdown.asp -->
<script>
// Set the date we're counting down to
var countDownDate = new Date('<?=$deadline->format('Y-m-d H:i:s')?>').getTime();


// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="countdown"
  document.getElementById("countdown").innerHTML = days + " day" + (days != 1 ? "s" : "") + ", " +
      (hours < 10 ? '0' : '') + hours + ":" + 
      (minutes < 10 ? '0' : '') + minutes + ":" + 
      (seconds < 10 ? '0' : '') + seconds;

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("countdown").innerHTML = "EXPIRED";
}
}, 1000);
</script>
