<?php

function agenda_period_for_display($iter) {
	// If there is no till date, leave it out
	if (!$iter['tot'] || $iter['tot'] == $iter['van']) {
		
		// There is no time specified
		if ($iter['vanuur'] + 0 == 0)
			$format = '$from_dayname $from_day|ordinal of $from_month';
		else
			$format = '$from_dayname $from_day|ordinal of $from_month, $from_time';
	}

	/* Check if date part (not time) is not the same */
	else if (substr($iter['van'], 0, 10) != substr($iter['tot'], 0, 10)) {
		$format = '$from_dayname $from_day|ordinal of $from_month $from_time till $till_dayname $till_day|ordinal of $till_month $till_time';
	} else {
		$format = '$from_dayname $from_day|ordinal of $from_month, $from_time till $till_time';
	}

	$days = Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$months = Array('no', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

	$van = new DateTime($iter['van']);
	$tot = new DateTime($iter['tot'] ? $iter['tot'] : $iter['van']);
	
	return format_string($format, array(
		'from_dayname' => $days[$van->format('w')],
		'from_day' => $van->format('j'),
		'from_month' => $months[$van->format('n')],
		'from_time' => $van->format('H:i'),
		'till_dayname' => $days[$tot->format('w')],
		'till_day' => $tot->format('j'),
		'till_month' => $months[$tot->format('n')],
		'till_time' => $tot->format('H:i')
	));
}

$cover_url = get_config_value('url_to_cover');

$json_data = file_get_contents($cover_url.'api.php/?method=agenda');
$agenda = json_decode($json_data, true);

// Only 10 items fit on the screen at the same time.
$punten = array_slice($agenda, 0, 10);

?>
<div style="overflow: hidden">
	<h1 style="text-indent: 20px;">Agenda</h1>
	<?php foreach ($punten as $punt): ?>
	<div style="display: block; float: left; width: 840px; height: 140px; margin: 0 20px 0 40px; padding: 20px; border-bottom: 1px solid #ccc">
		<h3><?=markup_format_text($punt['kop'])?></h3>
		<span class="date"><?=agenda_period_for_display($punt) ?></span>
		<?php if ($punt['locatie']): ?>
			<span class="location">in <?= markup_format_text($punt['locatie']) ?></span>
		<?php endif ?>
	</div>
	<?php endforeach ?>
</div>
