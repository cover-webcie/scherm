<?php
$commissie_model = get_model('DataModelCommissie');
$commissie_model->type = DataModelCommissie::TYPE_COMMITTEE;

// for debugging purposes
if (isset($_GET['commissie'])) {
	$commissie = $commissie_model->get_iter($_GET['commissie']);
}
// Pick a random commissie
else {
	$commissie = $commissie_model->get_random();
}

$leden = $commissie->get_members();

function _full_name($lid) {
	return $lid->get('tussenvoegsel')
		? sprintf('%s %s %s', $lid->get('voornaam'), $lid->get('tussenvoegsel'), $lid->get('achternaam'))
		: sprintf('%s %s', $lid->get('voornaam'), $lid->get('achternaam'));
}

function _translate_function_name($function_name){
	require 'translations.php';

	if(array_key_exists($function_name, $translations)){
		return $translations[$function_name];
	}
	return $function_name;
}

function _find_image($files) {
	$photo_path = get_config_value('path_to_committee_photos');
	$cover_url = get_config_value('url_to_cover');
	foreach ($files as $file)
		if (file_exists($photo_path . $file))
			return $cover_url . 'images/committees/' . $file;
	return null;
}

$commissie_foto_url = _find_image(array(
	$commissie->get('login') . '.gif',
	$commissie->get('login') . '.jpg'
));

$cover_url = get_config_value('url_to_cover');
$member_photo_url = edit_url($cover_url.'foto.php', array(
	'format' => 'square',
	'width' => 300
));

?>
<div style="text-align: center; width:100%;height:100%; position:relative; overflow:hidden;">
	<?php if($commissie_foto_url): ?>
		<!-- Committee photo -->
		<div style="position:absolute; top:-30px; right:-30px; bottom:-30px; left:-30px; background: black;"></div>
		<div style="position:absolute; top:0; right:0; bottom:0; left:0; background:url('<?= $commissie_foto_url ?>') center/contain no-repeat; filter:drop-shadow(0 0 100px black); -webkit-filter:drop-shadow(0 0 100px black);">
			<h1 class="text-outline-thick-white" style="font-size: 80px;"><?=markup_format_text($commissie->get('naam'))?></h1>
			<table style="position:absolute; bottom:20px; left:20px; width:auto; color:white; text-align:left; text-shadow: 0 0 3px rgba(0, 0, 0, 0.8);">
			<?php foreach ($leden as $lid): ?>
				<tr>
					<td style="font-size: 20px; text-transform:lowercase; font-variant:small-caps; vertical-align:baseline; text-align:right; padding-right:20px;"><?=markup_format_text(_translate_function_name($lid->get('functie')))?></td>
					<td style="font-size: 30px; vertical-align:baseline; text-align: left;"><?=markup_format_text(_full_name($lid))?></td>
				</tr>
			<?php endforeach ?>
			</table>
		</div>

	<?php else: ?>
		<!-- Committee faces -->
		<h1 style="font-size: 80px;"><?=markup_format_text($commissie->get('naam'))?></h1>
		<?php foreach ($leden as $lid): ?>
<?php
?>
		<div style="display: inline-block; padding: 25px 0; position: relative">
			<div style="position:absolute;width:298px;height:298px;border-radius:50%; border:1px solid rgba(0,0,0,0.1); margin: 0 50px;"></div>
			<img src="<?= edit_url($member_photo_url, array('lid_id' => $lid->get('id'))) ?>" width="300" height="300" style="border-radius:50%; margin: 0 50px;">
			<span style="display: block; font-size: 30px; line-height: 0.9em;"><?=markup_format_text(_full_name($lid))?></span>
			<span style="display: block; font-size: 20px; text-transform:lowercase; font-variant:small-caps;"><?=markup_format_text(_translate_function_name($lid->get('functie')))?></span>
		</div>
		<?php endforeach ?>
	<?php endif ?>

 	<?php if ($commissie->has_vacancies()): ?>
		<!-- New committee members banner -->
		<div style="position: absolute; top: 100px; left: -175px; width: 500px; padding: 20px 100px; border-radius: 10px; background:#c60c30; box-shadow: 0 0 0 4px white, 0 0 0 8px #c60c30; font-size: 50px; line-height: 1em; text-align: center; color: white; transform: rotate(-45deg); -webkit-transform: rotate(-45deg);">
			Looking for new members!
		</div>
	<?php endif ?> 
</div>
