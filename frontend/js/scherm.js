var slides = document.getElementsByClassName('slide');

var currentETag = null;

function checkETag(request)
{
	var etag = request.getResponseHeader('X-Scherm-ETag');

	if (etag === null)
		return;

	if (currentETag === null)
		return currentETag = etag;

	if (currentETag != etag)
		document.location.reload();
}

function load_slide(slide, callback)
{
	var request = new XMLHttpRequest();
	request.open('GET', slide.getAttribute('data-url'), true);
	request.onreadystatechange = function() {
		slide.setAttribute('data-ready-state', 'loading');

		if (request.readyState == 4) {
			checkETag(request);

			slide.classList.remove('ready-state-loading');

			if (request.status == 200) {
				slide.setAttribute('data-ready-state', 'loaded');
				slide.innerHTML = request.responseText;

				// Evaluate all the JavaScript code blocks
				Array.prototype.forEach.call(
					slide.querySelectorAll('script'),
					function(script) {
						new Function(script.innerHTML).call(slide);
					});
			} else {
				// Skip the slide
				slide.setAttribute('data-ready-state', 'failed');
			}

			if (callback)
				callback(slide);
		}
	};
	request.send();
}

function delay_load_slide(slide, delay, retries)
{
	if (retries === undefined)
		retries = 0;
	
	function retry_callback(slide) {
		if (slide.getAttribute('data-ready-state') == 'failed' && retries < slides.length) {
			var next = slides[(Array.prototype.indexOf.call(slides, slide) + 1) % slides.length];
			delay_load_slide(next, 500, retries + 1);
		}
	}

	setTimeout(function() { load_slide(slide, retry_callback); }, delay);
}

function next_slide()
{
	// Find the current slide
	for (var i = 0; i < slides.length; ++i)
		if (slides[i].classList.contains('current'))
			break;

	if (i === slides.length)
		throw new Error('Could not find current slide');


	// Find the next slide that hasn't failed to load
	for (var j = 1; j < slides.length; ++j) {
		var next = (i + j) % slides.length;
		if (slides[next].getAttribute('data-ready-state') != 'failed')
			break;
	}

	// Make the next slide the current slide
	slides[i].classList.remove('current');
	slides[next].classList.add('current');

	// Update progress bar
	var progress = document.getElementById('progress-bar');
	progress.style.width = 100*(next+1) / slides.length + '%';

	// Clear the current slide (to save resources)
	slides[i].innerHTML = '';

	// Queue the transition to the next slide
	queue_next_slide(slides[i].getAttribute('data-time'));

	// And schedule loading of the one after that
	delay_load_slide(slides[(i + j + 1) % slides.length], 1000);
}

function queue_next_slide(timeout)
{
	if (window.next_slide_timer)
		clearTimeout(window.next_slide_timer);

	if (!timeout)
		timeout = 20;

	window.next_slide_timer = setTimeout(next_slide, parseInt(timeout, 10) * 1000);
}

function layout_photos(slide)
{
	$('.flow-gallery').each(function() {
		var $area = $(this);

		var items = $(this).find('li').map(function() {
			var $thumb = $(this).find('img').first(),
				tw = parseInt($thumb.attr('width')),
				th = parseInt($thumb.attr('height'));

			// Scale height to 3rd of window height
			var theight = parseInt(window.innerHeight / 3),
				twidth = tw * theight / th;

			return {
				'twidth': twidth,
				'theight': theight,
				'title': '',
				'el': $(this)
			};
		}).get();

		FlowGallery.showImages($area, items);
	});
}

function scroll_slide(slide)
{
	var container = slide.querySelector('ul');
	var diff = container.scrollHeight - window.innerHeight;
	container.style.webkitTransition = '-webkit-transform 19s linear';
	container.style.webkitTransform = 'translate(0, -' + Math.max(diff, 0) + 'px)';
	container.style.mozTransition = '-moz-transform 19s linear';
	container.style.mozTransform = 'translate(0, -' + Math.max(diff, 0) + 'px)';
	container.style.transition = 'transform 19s linear';
	container.style.transform = 'translate(0, -' + Math.max(diff, 0) + 'px)';
}

function reset_scroll_slide(slide)
{
	var container = slide.querySelector('ul');
	container.style.webkitTransition = 'none';
	container.style.webkitTransform = 'translate(0, 0)';
	container.style.mozTransition = 'none';
	container.style.mozTransform = 'translate(0, 0)';
	container.style.transition = 'none';
	container.style.transform = 'translate(0, 0)';
}

// Load the first slide
load_slide(slides[0]);
slides[0].classList.add('current');

// Queue the next one
if (slides.length > 1) {
	// Load that next slide (delayed so it doesnt interfere with the current one)
	// because next_slide will only load the one after the one it switches to.
	delay_load_slide(slides[1], 1000);
	queue_next_slide(slides[0].getAttribute('data-time'));
}

// Allow to go to the next slide by pressing the right arrow key
document.addEventListener('keydown', function(e) {
	if (e.keyCode != 39)
		return;

	next_slide();
	e.preventDefault();
});
