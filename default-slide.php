<?php

$posters = glob('./*.{jpg,png,svg,gif}', GLOB_BRACE);

$poster_folder = basename(getcwd());

$poster_cookie = $poster_folder . '_idx';

// 'Restore' the current poster
if (isset($_COOKIE[$poster_cookie]))
	$index = min(max(0, intval($_COOKIE[$poster_folder . '_idx'])), count($posters) - 1);
else
	$index = mt_rand(0, count($posters) - 1);

// Go to the next poster
$index = ($index + 1) % count($posters);

$poster = $posters[$index];

// Save the index
setcookie($poster_cookie, (string) $index, 0);

?>
<img src="<?=$this->link_resource($poster)?>" width="100%">
